# Makefile for the OpenMoko FSO development system
# Licensed under the GPL v2 or later

BITBAKE_VERSION = branches/bitbake-1.8

FSO_STABLE_MILESTONE = milestone5.5
FSO_STABLE_BRANCH = fso/${FSO_STABLE_MILESTONE}
FSO_STABLE_VERSION = fso/${FSO_STABLE_MILESTONE}

.PHONY: all
all: update build

.PHONY: setup
setup:  setup-common setup-bitbake setup-openembedded \
	setup-fso-unstable setup-fso-testing setup-fso-${FSO_STABLE_MILESTONE}

.PHONY: prefetch
prefetch: prefetch-fso-unstable prefetch-fso-testing prefetch-fso-${FSO_STABLE_MILESTONE}

.PHONY: update
update:
	[ ! -e common ] || ${MAKE} update-common
	[ ! -e bitbake ] || ${MAKE} update-bitbake
	[ ! -e openembedded ] || ${MAKE} update-openembedded
	[ ! -e fso-unstable ] || ${MAKE} update-fso-unstable
	[ ! -e fso-testing ] || ${MAKE} update-fso-testing
	[ ! -e fso-${FSO_STABLE_MILESTONE} ] || ${MAKE} update-fso-${FSO_STABLE_MILESTONE}

.PHONY: build
build:
	[ ! -e fso-unstable ] || ${MAKE} fso-unstable-image
	[ ! -e fso-testing ] || ${MAKE} fso-testing-image
	[ ! -e fso-${FSO_STABLE_MILESTONE} ] || ${MAKE} fso-${FSO_STABLE_MILESTONE}-image
	[ ! -e fso-unstable ] || ${MAKE} fso-unstable-packages
	[ ! -e fso-testing ] || ${MAKE} fso-testing-packages
	[ ! -e fso-${FSO_STABLE_MILESTONE} ] || ${MAKE} fso-${FSO_STABLE_MILESTONE}-packages

.PHONY: status
status: status-common status-bitbake status-openembedded

.PHONY: clobber
clobber: clobber-fso-unstable clobber-fso-testing clobber-fso-${FSO_STABLE_MILESTONE}

.PHONY: distclean
distclean: distclean-bitbake distclean-openembedded \
	 distclean-fso-unstable distclean-fso-testing distclean-fso-${FSO_STABLE_MILESTONE}

.PHONY: prefetch-%
prefetch-%: %/.configured
	( cd $* ; ${MAKE} prefetch )

.PHONY: fso-unstable-image
fso-unstable-image: fso-gta01-unstable-image fso-gta02-unstable-image fso-a780-unstable-image

.PHONY: fso-testing-image
fso-testing-image: fso-gta01-testing-image fso-gta02-testing-image fso-a780-testing-image

.PHONY: fso-${FSO_STABLE_MILESTONE}-image
fso-${FSO_STABLE_MILESTONE}-image: fso-gta01-${FSO_STABLE_MILESTONE}-image fso-gta02-${FSO_STABLE_MILESTONE}-image fso-a780-${FSO_STABLE_MILESTONE}-image

.PHONY: fso-gta01-%-image
fso-gta01-%-image: fso-%/.configured
	( cd fso-$* ; \
	  ${MAKE} setup-image-fso-paroli-image ; \
	  ${MAKE} setup-machine-om-gta01 ; \
	  ${MAKE} -k image )

.PHONY: fso-gta02-%-image
fso-gta02-%-image: fso-%/.configured
	( cd fso-$* ; \
	  ${MAKE} setup-image-fso-paroli-image ; \
	  ${MAKE} setup-machine-om-gta02 ; \
	  ${MAKE} -k image )

.PHONY: fso-a780-%-image
fso-a780-%-image: fso-%/.configured
	( cd fso-$* ; \
	  ${MAKE} setup-image-fso-paroli-image ; \
	  ${MAKE} setup-machine-a780 ; \
	  ${MAKE} -k image )

.PHONY: fso-%-packages
fso-%-packages: fso-gta01-%-packages fso-gta02-%-packages fso-a780-%-packages

.PHONY: fso-gta01-%-packages
fso-gta01-%-packages: fso-%/.configured
	( cd fso-$* ; \
	  ${MAKE} setup-image-fso-paroli-image ; \
	  ${MAKE} setup-machine-om-gta01 ; \
	  ${MAKE} -k distro index )

.PHONY: fso-gta02-%-packages
fso-gta02-%-packages: fso-%/.configured
	( cd fso-$* ; \
	  ${MAKE} setup-image-fso-paroli-image ; \
	  ${MAKE} setup-machine-om-gta02 ; \
	  ${MAKE} -k distro index )

.PHONY: fso-a780-%-packages
fso-a780-%-packages: fso-%/.configured
	( cd fso-$* ; \
	  ${MAKE} setup-image-fso-paroli-image ; \
	  ${MAKE} setup-machine-a780 ; \
	  ${MAKE} -k distro index )

.PHONY: fso-%-index
fso-%-index: fso-%/.configured
	( cd fso-$* ; \
	  ${MAKE} setup-image-fso-paroli-image ; \
	  ${MAKE} -k index)

.PHONY: setup-common
.PRECIOUS: common/.git/config
setup-common common/.git/config:
	[ -e common/.git/config ] || \
	( git clone git://git.freesmartphone.org/fso-makefile.git common && \
	  rm -f Makefile && \
	  ln -s common/Makefile Makefile )
	touch common/.git/config

.PHONY: setup-bitbake
.PRECIOUS: bitbake/.svn/entries
setup-bitbake bitbake/.svn/entries:
	[ -e bitbake/.svn/entries ] || \
	( svn co svn://svn.berlios.de/bitbake/${BITBAKE_VERSION} bitbake )
	touch bitbake/.svn/entries

.PHONY: setup-openembedded
.PRECIOUS: openembedded/.git/config
setup-openembedded openembedded/.git/config:
	[ -e openembedded/.git/config ] || \
	( git clone git://git.openembedded.net/openembedded openembedded )
	( cd openembedded && \
	  ( git branch | egrep -e ' org.openembedded.dev$$' > /dev/null || \
	    git checkout -b org.openembedded.dev --track origin/org.openembedded.dev ))
	( cd openembedded && git checkout org.openembedded.dev )
	touch openembedded/.git/config

.PHONY: setup-%
setup-%:
	${MAKE} $*/.configured

.PRECIOUS: fso-${FSO_STABLE_MILESTONE}/.configured
fso-${FSO_STABLE_MILESTONE}/.configured: common/.git/config bitbake/.svn/entries openembedded/.git/config
	[ -d fso-${FSO_STABLE_MILESTONE} ] || ( mkdir -p fso-${FSO_STABLE_MILESTONE} )
	[ -e downloads ] || ( mkdir -p downloads )
	[ -e fso-${FSO_STABLE_MILESTONE}/Makefile ] || ( cd fso-${FSO_STABLE_MILESTONE} ; ln -sf ../common/openembedded.mk Makefile )
	[ -e fso-${FSO_STABLE_MILESTONE}/setup-env ] || ( cd fso-${FSO_STABLE_MILESTONE} ; ln -sf ../common/setup-env . )
	[ -e fso-${FSO_STABLE_MILESTONE}/downloads ] || ( cd fso-${FSO_STABLE_MILESTONE} ; ln -sf ../downloads . )
	[ -e fso-${FSO_STABLE_MILESTONE}/bitbake ] || ( cd fso-${FSO_STABLE_MILESTONE} ; ln -sf ../bitbake . )
	[ -e fso-${FSO_STABLE_MILESTONE}/openembedded ] || \
	( cd fso-${FSO_STABLE_MILESTONE} ; \
	  git clone --reference ../openembedded git://git.openembedded.net/openembedded openembedded; \
	  cd openembedded ; \
	  git checkout --no-track -b ${FSO_STABLE_BRANCH} origin/${FSO_STABLE_BRANCH}; \
	  git reset --hard origin/${FSO_STABLE_VERSION} )
	[ -d fso-${FSO_STABLE_MILESTONE}/conf ] || ( mkdir -p fso-${FSO_STABLE_MILESTONE}/conf )
	[ -e fso-${FSO_STABLE_MILESTONE}/conf/site.conf ] || ( cd fso-${FSO_STABLE_MILESTONE}/conf ; ln -sf ../../common/conf/site.conf . )
	[ -e fso-${FSO_STABLE_MILESTONE}/conf/auto.conf ] || ( \
		echo "DISTRO = \"openmoko\"" > fso-${FSO_STABLE_MILESTONE}/conf/auto.conf ; \
		echo "MACHINE = \"om-gta02\"" >> fso-${FSO_STABLE_MILESTONE}/conf/auto.conf ; \
		echo "IMAGE_TARGET = \"fso-paroli-image\"" >> fso-${FSO_STABLE_MILESTONE}/conf/auto.conf ; \
		echo "DISTRO_TARGET = \"openmoko-feed\"" >> fso-${FSO_STABLE_MILESTONE}/conf/auto.conf ; \
		echo "INHERIT += \"rm_work\"" >> fso-${FSO_STABLE_MILESTONE}/conf/auto.conf ; \
	)
	[ -e fso-${FSO_STABLE_MILESTONE}/conf/local.conf ] || ( \
		echo "# require conf/distro/include/moko-autorev.inc" > fso-${FSO_STABLE_MILESTONE}/conf/local.conf ; \
		echo "# require conf/distro/include/fso-autorev.inc" >> fso-${FSO_STABLE_MILESTONE}/conf/local.conf ; \
	)
	rm -rf fso-${FSO_STABLE_MILESTONE}/tmp/cache
	touch fso-${FSO_STABLE_MILESTONE}/.configured

.PRECIOUS: fso-testing/.configured
fso-testing/.configured: common/.git/config bitbake/.svn/entries openembedded/.git/config
	[ -d fso-testing ] || ( mkdir -p fso-testing )
	[ -e downloads ] || ( mkdir -p downloads )
	[ -e fso-testing/Makefile ] || ( cd fso-testing ; ln -sf ../common/openembedded.mk Makefile )
	[ -e fso-testing/setup-env ] || ( cd fso-testing ; ln -sf ../common/setup-env . )
	[ -e fso-testing/downloads ] || ( cd fso-testing ; ln -sf ../downloads . )
	[ -e fso-testing/bitbake ] || ( cd fso-testing ; ln -sf ../bitbake . )
	[ -e fso-testing/openembedded ] || ( cd fso-testing ; ln -sf ../openembedded . )
	[ -d fso-testing/conf ] || ( mkdir -p fso-testing/conf )
	[ -e fso-testing/conf/site.conf ] || ( cd fso-testing/conf ; ln -sf ../../common/conf/site.conf . )
	[ -e fso-testing/conf/auto.conf ] || ( \
		echo "DISTRO = \"openmoko\"" > fso-testing/conf/auto.conf ; \
		echo "MACHINE = \"om-gta02\"" >> fso-testing/conf/auto.conf ; \
		echo "IMAGE_TARGET = \"fso-paroli-image\"" >> fso-testing/conf/auto.conf ; \
		echo "DISTRO_TARGET = \"openmoko-feed\"" >> fso-testing/conf/auto.conf ; \
		echo "INHERIT += \"rm_work\"" >> fso-testing/conf/auto.conf ; \
	)
	[ -e fso-testing/conf/local.conf ] || ( \
		echo "# require conf/distro/include/moko-autorev.inc" > fso-testing/conf/local.conf ; \
		echo "# require conf/distro/include/fso-autorev.inc" >> fso-testing/conf/local.conf ; \
	)
	rm -rf fso-testing/tmp/cache
	touch fso-testing/.configured

.PRECIOUS: fso-unstable/.configured
fso-unstable/.configured: common/.git/config bitbake/.svn/entries openembedded/.git/config
	[ -d fso-unstable ] || ( mkdir -p fso-unstable )
	[ -e downloads ] || ( mkdir -p downloads )
	[ -e fso-unstable/Makefile ] || ( cd fso-unstable ; ln -sf ../common/openembedded.mk Makefile )
	[ -e fso-unstable/setup-env ] || ( cd fso-unstable ; ln -sf ../common/setup-env . )
	[ -e fso-unstable/downloads ] || ( cd fso-unstable ; ln -sf ../downloads . )
	[ -e fso-unstable/bitbake ] || ( cd fso-unstable ; ln -sf ../bitbake . )
	[ -e fso-unstable/openembedded ] || ( cd fso-unstable ; ln -sf ../openembedded . )
	[ -d fso-unstable/conf ] || ( mkdir -p fso-unstable/conf )
	[ -e fso-unstable/conf/site.conf ] || ( cd fso-unstable/conf ; ln -sf ../../common/conf/site.conf . )
	[ -e fso-unstable/conf/auto.conf ] || ( \
		echo "DISTRO = \"openmoko\"" > fso-unstable/conf/auto.conf ; \
		echo "MACHINE = \"om-gta02\"" >> fso-unstable/conf/auto.conf ; \
		echo "IMAGE_TARGET = \"fso-paroli-image\"" >> fso-unstable/conf/auto.conf ; \
		echo "DISTRO_TARGET = \"openmoko-feed\"" >> fso-unstable/conf/auto.conf ; \
		echo "INHERIT += \"rm_work\"" >> fso-unstable/conf/auto.conf ; \
	)
	[ -e fso-unstable/conf/local.conf ] || ( \
		echo "require conf/distro/include/moko-autorev.inc" > fso-unstable/conf/local.conf ; \
		echo "require conf/distro/include/fso-autorev.inc" >> fso-unstable/conf/local.conf ; \
	)
	rm -rf fso-unstable/tmp/cache
	touch fso-unstable/.configured

.PHONY: update-common
update-common: common/.git/config
	( cd common ; git pull )

.PHONY: update-bitbake
update-bitbake: bitbake/.svn/entries
	( cd bitbake ; svn up )

.PHONY: update-openembedded
update-openembedded: openembedded/.git/config
	( cd openembedded ; git pull )

.PHONY: update-fso-unstable
update-fso-unstable: fso-unstable/.configured

.PHONY: update-fso-testing
update-fso-testing: fso-testing/.configured

.PHONY: update-fso-${FSO_STABLE_MILESTONE}
update-fso-${FSO_STABLE_MILESTONE}: fso-${FSO_STABLE_MILESTONE}/.configured
	( cd fso-${FSO_STABLE_MILESTONE}/openembedded ; \
	  git fetch ; \
	  git checkout ${FSO_STABLE_BRANCH} ; \
	  git reset --hard origin/${FSO_STABLE_VERSION} )

.PHONY: status-common
status-common: common/.git/config
	( cd common ; git diff --stat )

.PHONY: status-bitbake
status-bitbake: bitbake/.svn/entries
	( cd bitbake ; svn status )

.PHONY: status-openembedded
status-openembedded: openembedded/.git/config
	( cd openembedded ; git diff --stat )

.PHONY: clobber-%
clobber-%:
	[ ! -e $*/Makefile ] || ( cd $* ; ${MAKE} clobber )

.PHONY: distclean-bitbake
distclean-bitbake:
	rm -rf bitbake

.PHONY: distclean-openembedded
distclean-openembedded:
	rm -rf openembedded

.PHONY: distclean-%
distclean-%:
	rm -rf $*

.PHONY: push
push: push-common

.PHONY: push-common
push-common: update-common
	( cd common ; git push ssh://git@git.freesmartphone.org/fso-makefile.git )

# End of Makefile
